import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { LayoutService } from './service/app.layout.service';

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit {

    model: any[] = [];

    constructor(public layoutService: LayoutService) { }

    ngOnInit() {
        this.model = [
            {
                label: 'Ward', icon: 'pi pi-home',
                items: [
                    { label: 'List Ward', icon: 'pi pi-building',                routerLink: ['/'] },
                  
                ]
            },
            {
                label: 'Patient', icon: 'pi pi-home',
                items: [
                    {  label: 'List Patient', icon: 'pi pi-users',                 routerLink: ['/list-patient'],  },
                 
                ]
            },
           

        ];
    }
}
