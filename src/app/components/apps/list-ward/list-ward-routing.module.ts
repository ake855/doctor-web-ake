import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListWardComponent} from './list-ward.component';

const routes: Routes = [
  { path: '', component: ListWardComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListWardRoutingModule { }


