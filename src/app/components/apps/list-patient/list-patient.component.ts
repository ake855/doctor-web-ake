import { Component, Renderer2 } from '@angular/core';
import {  CdkDragEnd, CdkDragStart } from '@angular/cdk/drag-drop';
import { NgxSpinnerService } from "ngx-spinner";
import { ActivatedRoute, Router } from '@angular/router';
import { Message, MessageService } from 'primeng/api';
import { SelectItem } from 'primeng/api';
import { DataView } from 'primeng/dataview';

import {ListPatientService} from './list-patient.service';



@Component({
  selector: 'app-list-patient',
  templateUrl: './list-patient.component.html',
  styleUrls: ['./list-patient.component.scss'],
  providers: [MessageService],

})
export class ListPatientComponent {

  
  query: any = '';
  dataSet: any[] = [];
  loading = false;
  patientList: any;

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;
  myEvent: string = 'middle';
  queryParamsData: any;
  item: any = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  private initialX = 0; 
  items:any = [];
  speedDialItems:any =[];
  deviceType: string = 'Unknown';
  filterName:string = '';
  sortOptions: SelectItem[] = [];

  sortOrder: number = 0;

  sortField: string = '';



  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private renderer: Renderer2,
    private messageService:MessageService,
    private listPatientService:ListPatientService,
    


  ) {
    let jsonString: any = this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    const localStorageWardId = localStorage.getItem('ward_id');
    if(jsonObject != null || jsonObject != undefined){
        this.queryParamsData = jsonObject;
    }else{
      this.queryParamsData = localStorageWardId;
    } 

    console.log(this.queryParamsData);

  }

  
  ngOnInit(): void {
    // this.user_login_name  =  this.userProfileService.user_login_name;
    this.user_login_name = sessionStorage.getItem('userLoginName');
    this.sortOptions = [
      { label: 'ก --> ฮ', value: 'fname' },
      { label: 'ฮ --> ก', value: '!fname' },
  ];
  this.buttonSpeedDial();
  
    this.getPatientAdmit();

  }
  /////////////เมธอด หลัก///////////ที่ต้องมี///////////////
  addItem(newItem: string) {
    this.items.push(newItem);
    console.log(this.item);
  }

 
  doSearch() {
    this.getPatientAdmit();
  }
  refresh() {
    this.query = '';
    this.pageIndex = 1;
    this.offset = 0;
    this.getPatientAdmit();
  }
  onSortChange(event: any) {
    const value = event.value;

    if (value.indexOf('!') === 0) {
        this.sortOrder = -1;
        this.sortField = value.substring(1, value.length);
    } else {
        this.sortOrder = 1;
        this.sortField = value;
    }
}

onFilter(dv: DataView, event: Event) {
    console.log(event);
    console.log((event.target as HTMLInputElement).value);
    dv.filter((event.target as HTMLInputElement).value);
}
clearFilter(dv: DataView, event: Event) {
    (event.target as HTMLInputElement).value = '';
    dv.filter((event.target as HTMLInputElement).value);
    this.filterName = ''; 
}



  onDragStarted(event: CdkDragStart,i:any) {
    this.initialX = event.source._dragRef.getFreeDragPosition().x;
  }

  onDragEnded(event: CdkDragEnd,i:any) {
    const finalX = event.source._dragRef.getFreeDragPosition().x; 
    if (finalX > this.initialX) {
      console.log('Dragged to the right');
      this.myEvent = 'right';
      this.patientList[i].hideLeft = false;   
      this.patientList[i].hideRight = true;
     

    } else if (finalX < this.initialX) {
      console.log('Dragged to the left');
      this.myEvent = 'left';
      this.patientList[i].hideRight = false;   
    this.patientList[i].hideLeft = true;

    } 
    
  }
  openPanelLeft( i: any) {
    this.patientList[i].hideLeft = false;   
    this.patientList[i].hideRight = true;

  }
  openPanelLeft2(i: any) {
    this.patientList[i].hideLeft = true;
    this.patientList[i].hideRight = true;
  }
  openPanelRight( i: any) {
    this.patientList[i].hideRight = false;   
    this.patientList[i].hideLeft = true;
  }
  openPanelRight2(i: any) {
    this.patientList[i].hideRight = true;
    this.patientList[i].hideLeft = true;
  }
  logOut() {
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }

  hideSpinner() {
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
  }


  beforeConfirm(): Promise<boolean> {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(true);
      }, 3000);
    });
  }
  buttonSpeedDial(){
    this.speedDialItems = [
      {
        icon: 'fa-solid fa-vial-virus',
        command: () => {
          this.navigateLab(this.patientId);

        },
        tooltipOptions: {
          tooltipLabel: 'Lab',
          tooltipPosition: 'left'
        },
      },
      
      {
        icon: 'fa-solid fa-phone-volume',
        command: () => {
          this.navigateConsult(this.patientId);

        },
        tooltipOptions: {
          tooltipLabel: 'Consult',
          tooltipPosition: 'left'
        },
      },
      
      {
        icon: 'fa-solid fa-heart-pulse',
        command: () => {
          this.navigateEkg(this.patientId);

        },
        tooltipOptions: {
          tooltipLabel: 'EKG',
          tooltipPosition: 'left'
        },
      },
      {
        icon: 'fa-solid fa-comment-medical',
        command: () => {
          this.navigateAdmisstionNote(this.patientId);
        },
        tooltipOptions: {
          tooltipLabel: 'Consult',
          tooltipPosition: 'left'
        },
      },
     
      {
        icon: 'fa-solid fa-circle-info',
        command: () => {
          this.navigatePatientInfo(this.patientId);
        },
        tooltipOptions: {
          tooltipLabel: 'Patient Info',
          tooltipPosition: 'left'
        },
      },
      {
        icon: 'fa-solid fa-user-doctor',
        command: () => {
          this.navigateDoctorOrder(this.patientId)
        },
        tooltipOptions: {
          tooltipLabel: 'Doctor Order',
          tooltipPosition: 'left'
        },
      },
     
    ];
  }


  //////////////////////

patientId:any;

  toggles(e:any,id:any){
    console.log(e);
    console.log(id);
    this.patientId = id;
  }
  onPageIndexChange(pageIndex: any) {

    this.offset = pageIndex === 1 ?
      0 : (pageIndex - 1) * this.pageSize;

    this.getPatientAdmit()
  }

  onPageSizeChange(pageSize: any) {
    this.pageSize = pageSize
    this.pageIndex = 1

    this.offset = 0

    this.getPatientAdmit()
  }

  async getPatientAdmit() {
    this.spinner.show();
    try {
      const response = await this.listPatientService.getPatientAdmit(this.queryParamsData);
      const data = await response.data;
      // console.log(data);
      this.patientList = data.data;
      this.patientList.forEach((patient: { hideLeft: boolean;hideRight: boolean; }) => {
        // Add the 'hide' field to each object and set its value to true
        patient.hideLeft = true;
        patient.hideRight = true;
      });
      console.log(this.patientList);

      this.hideSpinner();
    } catch (error: any) {
      console.log(error);
      this.hideSpinner();
      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด !',
        detail: 'กรูณาตรวจสอบ'+ error,
    });


    }
  }

 
  navigatePatientInfo(data:any){
    console.log(data) ;
    let jsonString = JSON.stringify(data);
    console.log(jsonString);   
    this.router.navigate(['/patient-info'], { queryParams: { data: jsonString } });

  }
  navigateDoctorOrder(data:any){
    console.log(data) ;
    let jsonString = JSON.stringify(data);
    console.log('admit id:',jsonString);   
    this.router.navigate(['/doctor-order'], { queryParams: { data: jsonString } });

  }
  navigateAdmisstionNote(data:any){
    console.log(data) ;
    let jsonString = JSON.stringify(data);
    console.log('send to admission note:',jsonString);   
    this.router.navigate(['/admission-note'], { queryParams: { data: jsonString } });

  }
  navigateProgressNote(data:any){
    console.log(data) ;
    let jsonString = JSON.stringify(data);
    console.log(jsonString);   
    this.router.navigate(['/list-patient'], { queryParams: { data: jsonString } });

  }
  navigateEkg(data:any){
    console.log(data) ;
    let jsonString = JSON.stringify(data);
    console.log(jsonString);   
    this.router.navigate(['/ekg'], { queryParams: { data: jsonString } });

  }

  
  navigateConsult(data:any){
    console.log('consult'+data) ;
    let jsonString = JSON.stringify(data);
    console.log(jsonString);   
    this.router.navigate(['/consult'], { queryParams: { data: jsonString } });
    // this.router.navigate(['/consult'], { state: { orderId: 1234 } });
    // this.router.navigate(['/consult'], {
    //   state: { exampleData: 'This is just an example' }
    // });
    // this.router.navigate(['/consult'], { state: { exampleData: 'This is an example' } });
    // this.router.navigate(['/consult'], { state: { param: 'xxx' } });


  }
  navigateLab(data:any){
    console.log('lab'+data) ;
    let jsonString = JSON.stringify(data);
    console.log(jsonString);   
    this.router.navigate(['/lab'], { queryParams: { data: jsonString } });

  }
}
