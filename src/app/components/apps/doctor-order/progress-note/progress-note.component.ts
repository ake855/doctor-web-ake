import { Component, OnInit, Input, AfterViewInit, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { DoctorOrderService } from '../doctor-order-service';
import { LookupService } from '../../../../shared/lookup-service';
import { ActivatedRoute, Router } from '@angular/router';
import { DateTime } from 'luxon';
import * as _ from 'lodash';

@Component({
    selector: 'app-progress-note',
    templateUrl: './progress-note.component.html',
    styleUrls: ['./progress-note.component.scss'],
})
export class ProgressNoteComponent implements OnInit, OnChanges {
    @Input() parentData: any;
    @Output() dataEvent = new EventEmitter<string>();
    @Output() child1Event = new EventEmitter<string>();
    sidebarVisible2: boolean = false;
    queryParamsData: any;
    dxData: any;
    pageSize = 20;
    pageIndex = 1;
    offset: any;
    total: any;
    dataSet: any;
    showDx: boolean = true;
    dxName: any;

    optionSubjective: any;
    optionSubjectiveSelected: any = [];

    optionObjective: any = [];
    optionObjectiveSelected: any = [];

    optionsAssessment: any;
    optionAssessmentSelected: any = [];

    optionPlan: any;
    optionPlanSelected: any = [];

    showProgressNote: boolean = false;

    progressValueS: any;
    progressValueO: any;
    progressValueA: any;
    progressValueP: any;
    progressValueN: any;

    progress_note_subjective: any[] = [];
    progress_note_objective: any[] = [];
    progress_note_assertment: any[] = [];
    progress_note_plan: any[] = [];
    progress_note: any[] = [];
    // progressValue: any = [];
    progressValue: { label: string }[] = [];
    dxLoading: boolean = false;

    //Order One
    //itemtypeID = 1
    orderOneday: any;
    orderOnedaySelected: any = [];

    orderOnedayV: any;
    orderOndayValue: any = [];
    selectedItemsOrderOneday: any = [];




    //Order Contines
    //itemtypeID = 1
    orderContine: any;
    orderContineSelected: any = [];
    selectedItemsOrdercontine: any = [];


    // doctor order id
    doctorOrderData: any;
    is_new_order: boolean = true;
    is_success: boolean = false;

    // user
    userData: any;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private lookupService: LookupService,
        private doctorOrderService: DoctorOrderService
    ) {
        let jsonString: any =
            this.activatedRoute.snapshot.queryParamMap.get('data');
        const jsonObject = JSON.parse(jsonString);
        this.queryParamsData = jsonObject;
        console.log('queryParamsData:', this.queryParamsData);

        let _data = sessionStorage.getItem('doctor_order');

        if (_data != 'undefined' && _data != null) {
            let _doctorOrderData = JSON.parse(_data);
            this.doctorOrderData = _doctorOrderData.doctor_order;
            console.log(this.doctorOrderData);
            this.is_new_order = false;
        } else {
            this.is_new_order = true;
        }

        let _user = sessionStorage.getItem('userData');
        if (_user != 'undefined' && _user != null) {
            let user = JSON.parse(_user!);
            this.userData = user;
        } else {
            this.userData = null;
        }
    }
    ngOnInit(): void {
        if(this.parentData != null || this.parentData != undefined){
            this.is_new_order = this.parentData.isNewOrder;
            this.doctorOrderData = this.parentData.doctor_order;
        }
    }
    ngOnChanges(changes: SimpleChanges) {
        if (changes['parentData'] && !changes['parentData'].firstChange) {

        }
    }
    sendData(orderData: any) {

        this.dataEvent.emit(orderData);
    }
    sendDataToParent(orderData: any) {
        console.log('me send');
        this.child1Event.emit(orderData);
    }
    getProgressNote() {
        // console.log('progressNote');
        this.sidebarVisible2 = true;

        this.getList();
        //this.getProgresnote();
        this.getDx();
    }

    async getDx() {
        this.dxLoading = true;
        try {
            let response = await this.doctorOrderService.getDxStardingorder();
            let _dxData = response.data.data;
            this.dxData = _.sortBy(_dxData, [
                function (o) {
                    return o.name;
                },
            ]);
            console.log(this.dxData);
            this.dxLoading = false;
        } catch (error: any) {
            console.log('getDx' + `${error.code} - ${error.message}`);
            this.dxLoading = false;
        }
    }
    async getList() {
        try {
            const _limit = this.pageSize;
            const _offset = this.offset;
            const response = await this.doctorOrderService.getWaiting(
                _limit,
                _offset
            );

            const data: any = response.data;
            // console.log('get doctor order :',data);

            this.total = data.total || 1;

            this.dataSet = data.data.map((v: any) => {
                const date = v.admit_date
                    ? DateTime.fromISO(v.admit_date)
                        .setLocale('th')
                        .toLocaleString(DateTime.DATETIME_SHORT_WITH_SECONDS)
                    : '';
                v.admit_date = date;
                return v;
            });
            // console.log(this.dataSet);
        } catch (error: any) {
            console.log(`${error.code} - ${error.message}`);
        }
    }

    async dxlistClick(id: any, name: any) {
        console.log('xx', id);

        try {
            //get Standing Progress note
            let response = await this.doctorOrderService.getStanding_progressnote(id);
            //get Standing Order 
            let responseOrder = await this.doctorOrderService.getlockupstandinginfoGroupDiseaseID(id);
            console.log('xxx', responseOrder);


            //  let progessnoteData: any = response.data.data;
            // this.optionsDrugsUsage = response.data.data;
            console.log(response);
            this.dxName = name;
            //show Progress Note
            let progessnoteData = response.data.data;
            this.optionSubjective = progessnoteData.filter(
                (item: any) => item.progress_note_type_code == 'S'
            );
            this.optionObjective = progessnoteData.filter(
                (item: any) => item.progress_note_type_code == 'O'
            );
            this.optionsAssessment = progessnoteData.filter(
                (item: any) => item.progress_note_type_code == 'A'
            );
            this.optionPlan = progessnoteData.filter(
                (item: any) => item.progress_note_type_code == 'P'
            );

            //show Order OneDay
            let l_orderOneday = responseOrder.data.data;
            this.orderOneday = l_orderOneday.filter(
                (orderODL: any) => orderODL.order_type_id == '1');
            //shw Order Contine
            let l_orderContine = responseOrder.data.data;
            this.orderContine = l_orderContine.filter(
                (orderODC: any) => orderODC.order_type_id == '2');


            this.showProgressNote = true;
            this.showDx = false;


        } catch (error: any) {
            console.log('getLabItems' + `${error.code} - ${error.message}`);
        }
    }


    optionSubjectClick(e: any) {
        console.log('e', e.checked[0]);

        if (e.checked.length > 0) {
            this.optionSubjectiveSelected.push(e.checked[0]);
        } else {
            this.optionSubjectiveSelected.splice(
                this.optionSubjectiveSelected.indexOf(e.checked[0]),
                1
            );
            // this.optionSubjectiveSelected.pop(e.target.value);
        }
        console.log(this.optionSubjectiveSelected);
    }
    optionObjectClick(e: any) {
        if (e.checked.length > 0) {
            this.optionObjectiveSelected.push(e.checked[0]);
        } else {
            this.optionObjectiveSelected.splice(
                this.optionObjectiveSelected.indexOf(e.checked[0]),
                1
            );
            // this.optionSubjectiveSelected.pop(e.target.value);
        }

        console.log(this.optionObjectiveSelected);
    }
    optionAssessmentClick(e: any) {
        if (e.checked.length > 0) {
            this.optionAssessmentSelected.push(e.checked[0]);
        } else {
            this.optionAssessmentSelected.splice(
                this.optionAssessmentSelected.indexOf(e.checked[0]),
                1
            );
            // this.optionSubjectiveSelected.pop(e.target.value);
        }

        console.log(this.optionAssessmentSelected);
    }

    optionPlanClick(e: any) {
        if (e.checked.length > 0) {
            this.optionPlanSelected.push(e.checked[0]);
        } else {
            this.optionPlanSelected.splice(
                this.optionPlanSelected.indexOf(e.checked[0]),
                1
            );
            // this.optionSubjectiveSelected.pop(e.target.value);
        }

        console.log(this.optionPlanSelected);
    }

    optionOrderOnedayClick(e: any) {
        console.log('oneday', e);

        if (e.checked.length > 0) {
            this.orderOnedaySelected.push(e.checked);
        } else {
            this.orderOnedaySelected.splice(
                this.orderOnedaySelected.indexOf(e.checked),
                1
            );
            // this.optionSubjectiveSelected.pop(e.target.value);
        }

        console.log(this.orderOnedaySelected);
    }
    optionOrderContineClick(e: any) {
        if (e.checked.length > 0) {
            this.orderContineSelected.push(e.checked);
        } else {
            this.orderContineSelected.splice(
                this.orderContineSelected.indexOf(e.checked),
                1
            );
            // this.optionSubjectiveSelected.pop(e.target.value);
        }

        console.log(this.orderContineSelected);
    }
    async checkOrder() {
        console.log('checkOrder');
        let _data = sessionStorage.getItem('doctor_order');

        if (_data != 'undefined' && _data != null) {
            let _doctorOrderData = JSON.parse(_data);           
            this.is_new_order = _doctorOrderData.isNewOrder;
            this.doctorOrderData = _doctorOrderData.doctor_order;

        } else {
            this.is_new_order = true;
        }
        console.log('is_new_order',this.is_new_order);

        if (this.is_new_order) {
            console.log('create new order');

            var date2 = DateTime.now();
            var date3 = DateTime.now();
            const date5 = date2.toFormat('yyyy-MM-dd');
            const time1 = date3.toFormat('HH:mm:ss');
            let data = {
                doctor_order: [{
                    admit_id: this.queryParamsData,
                    doctor_order_date: date5,
                    doctor_order_time: time1,
                    doctor_order_by: this.userData.id || null,
                    is_confirm: false,
                }],
            };
            console.log('send data:', data);
            try {
                let rs = await this.doctorOrderService.saveDoctorOrder(data);
                console.log('response:', rs);
                this.doctorOrderData = rs.data.data.doctor_order;
                this.is_new_order = false;              
                console.log('doctorOrderData:', this.doctorOrderData);
                this.is_success = true;
                let parentData = { 'isNewOrder': false, 'doctor_order': this.doctorOrderData };
                sessionStorage.setItem(
                    'doctor_order',
                    JSON.stringify(parentData)
                );
            } catch (error) {
                console.log('save Doctor Order:' + error);
                this.is_success = false;
            }
        } else {
            console.log('Use old order');
            let parentData = { 'isNewOrder': false, 'doctor_order': this.doctorOrderData };
            // this.sendData(parentData);
            // this.sendDataToParent(parentData);



            this.is_success = true;
        }
    }

    async addProgressNote(type: string) {
        await this.checkOrder(); // check if doctor order is exist

        let progress_note: any; // set progress note
        if (this.is_success) {
            // select type of progress note
            if (type == 'S') {
                this.progress_note_subjective.push({
                    label: this.progressValueS,
                });
                let _progress_note: any = [];
                for (let i of this.progress_note_subjective) {
                    _progress_note.push(i.label);
                }
                progress_note = {
                    subjective: _progress_note,
                };
                this.progressValueS = '';
            } else if (type == 'O') {
                this.progress_note_objective.push({
                    label: this.progressValueO,
                });
                let _progress_note: any = [];
                for (let i of this.progress_note_objective) {
                    _progress_note.push(i.label);
                }
                progress_note = {
                    objective: _progress_note,
                };
                this.progressValueO = '';
            } else if (type == 'A') {
                this.progress_note_assertment.push({
                    label: this.progressValueA,
                });
                let _progress_note: any = [];
                for (let i of this.progress_note_assertment) {
                    _progress_note.push(i.label);
                }
                progress_note = {
                    assertment: _progress_note,
                };
                this.progressValueA = '';
            } else if (type == 'P') {
                console.log(this.progressValueP);
                this.progress_note_plan.push({ label: this.progressValueP });
                let _progress_note: any = '';

                for (let i of this.progress_note_plan) {
                    _progress_note = [_progress_note + i.label];
                }

                // _progress_note = _progress_note + this.progressValueP ;

                progress_note = {
                    plan: _progress_note,
                };
                this.progressValueP = '';
            } else {
                this.progress_note.push({ label: this.progressValueN });
                let _progress_note: any = [];
                for (let i of this.progress_note) {
                    _progress_note.push(i.label);
                }
                progress_note = {
                    note: _progress_note,
                };
                this.progressValueN = '';
            }

            try {
                const response = await this.saveProgressNote(progress_note); // save progress note
                console.log(response);
                let parentData = { 'isNewOrder': false, 'doctor_order': this.doctorOrderData };

                sessionStorage.setItem(
                    'doctor_order',
                    JSON.stringify(parentData)
                );
            } catch (error) {
                console.log('save progress note:' + error);
            }
        } else {
            alert('ไม่สามารถบันทึกข้อมูลได้');
        }
    }

    async addProgressNote_standingOrder() {

        await this.checkOrder(); // check if doctor order is exist

        //เก็บค่าเฉพราะรายการที่เลืิิอกเท่านั้น
        if (this.is_success) {
            // select type of progress note
            let progress_note: any = [];
            let data = {
                "subjective": this.optionSubjectiveSelected,
                "objective": this.optionObjectiveSelected,
                "assertment": this.optionAssessmentSelected,
                "plan": this.optionPlanSelected

            }
            progress_note.push(data);
            console.log('saveprogress_note', progress_note);



            try {
                const response = await this.saveProgressNote(progress_note); // save progress note
                console.log(response);
            } catch (error) {
                console.log('save progress note:' + error);
            }
        } else {
            alert('ไม่สามารถบันทึกข้อมูลได้');
        }
    }
    async saveProgressNote(datas: any) {
        // set data to save

        let data = {
            doctor_order: this.doctorOrderData,
            progress_note: [datas],
        };
        console.log('progress_note:', data);

        try {
            const response = await this.doctorOrderService.saveDoctorOrder(
                data
            );
            console.log(response);
        } catch (error) {
            console.log('saveDoctorOrder:' + error);
        }
    }
    async removeProgressNote(label: string, type: string) {
        if (type == 'S') {
            this.progress_note_subjective =
                this.progress_note_subjective.filter(
                    (item) => item.label !== label
                );
            await this.addProgressNote('S');
        } else if (type == 'O') {
            this.progress_note_objective = this.progress_note_objective.filter(
                (item) => item.label !== label
            );
            await this.addProgressNote('O');
        } else if (type == 'A') {
            this.progress_note_assertment =
                this.progress_note_assertment.filter(
                    (item) => item.label !== label
                );
            await this.addProgressNote('A');
        } else if (type == 'P') {
            this.progress_note_plan = this.progress_note_plan.filter(
                (item) => item.label !== label
            );
            await this.addProgressNote('P');
        } else {
            this.progress_note = this.progress_note.filter(
                (item) => item.label !== label
            );
            await this.addProgressNote('N');
        }
    }



    async saveStandingOrder() {
        alert('saveStandingOrder');
        //กำหนดให้เป็น Order ใหม่
        this.is_new_order = true
        this.addProgressNote_standingOrder();
        this.addOrderOneday_standingOrder();
        this.addOrderContine_standingOrder();
        this.sidebarVisible2 = false;
    }


    // add order oneday
    async addOrderOneday_standingOrder() {
        if (this.orderOnedaySelected != '') {
            console.log('YYY');

            let orderData: any = [];
            for (let i of this.orderOnedaySelected) {
                this.filterStandingorderoneday(i[0])
                let data = {
                    order_type_id: 1,
                    item_type_id: this.selectedItemsOrderOneday.item_type_id,
                    item_id: this.selectedItemsOrderOneday.item_id,
                    item_name: this.selectedItemsOrderOneday.item_name,
                    medicine_usage_code: this.selectedItemsOrderOneday.medicine_usage || null,
                    medicine_usage_extra: this.selectedItemsOrderOneday.medicine_usage_extra || null,
                    quantity: Number(this.selectedItemsOrderOneday.quantity) || 1,
                    is_confirm: false,
                }
                orderData.push(data);
                console.log('add oneday', orderData);
                await this.saveOrderOneday(orderData);

            }
        }
    }

    async saveOrderOneday(datas: any) {
        // set data to save
        let data = {
            doctor_order: this.doctorOrderData,
            orders: datas,
        };
        console.log('save oneday:', data);

        try {
            const response = await this.doctorOrderService.saveDoctorOrder(
                data
            );
            console.log(response);
        } catch (error) {
            console.log('saveDoctorOrder_oneday:' + error);
        }
    }

    filterStandingorderoneday(id: any) {
        let filtered: any[] = _.find(this.orderOneday, { 'id': id });
        this.selectedItemsOrderOneday = filtered;
    }

    async addOrderContine_standingOrder() {
        if (this.orderContineSelected != '') {
            let orderData: any = [];
            for (let i of this.orderContineSelected) {
                this.filterStandingordercontine(i[0])
                let data = {
                    order_type_id: 2,
                    item_type_id: this.selectedItemsOrdercontine.item_type_id,
                    item_id: this.selectedItemsOrdercontine.item_id,
                    item_name: this.selectedItemsOrdercontine.item_name,
                    medicine_usage_code: this.selectedItemsOrdercontine.medicine_usage || null,
                    medicine_usage_extra: this.selectedItemsOrdercontine.medicine_usage_extra || null,
                    quantity: Number(this.selectedItemsOrdercontine.quantity) || 1,
                    is_confirm: false,
                }
                orderData.push(data);
                console.log('showorder_con', orderData);
                await this.saveOrderContine(orderData);

            }
        }
    }

    async saveOrderContine(datas: any) {
        // set data to save
        let data = {
            doctor_order: this.doctorOrderData,
            orders: datas,
        };

        try {
            const response = await this.doctorOrderService.saveDoctorOrder(
                data
            );
            console.log(response);
        } catch (error) {
            console.log('saveDoctorOrder:' + error);
        }
    }
    filterStandingordercontine(id: any) {
        let filtered: any[] = _.find(this.orderContine, { 'id': id });
        this.selectedItemsOrdercontine = filtered;
    }

}
